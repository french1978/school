-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Mar 18, 2015 at 07:24 PM
-- Server version: 5.6.19-log
-- PHP Version: 5.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `school`
--

-- --------------------------------------------------------

--
-- Table structure for table `school_student`
--

CREATE TABLE IF NOT EXISTS `school_student` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `school_student`
--

INSERT INTO `school_student` (`id`, `name`) VALUES
(3, 'Андрей'),
(4, 'Харитон'),
(5, 'Ученик'),
(6, 'Ещё ученик'),
(7, 'Георгий');

-- --------------------------------------------------------

--
-- Table structure for table `school_teacher`
--

CREATE TABLE IF NOT EXISTS `school_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `school_teacher`
--

INSERT INTO `school_teacher` (`id`, `name`) VALUES
(19, 'Учитель'),
(20, 'Ущё учитель'),
(21, 'Борис'),
(22, 'Иван Иванивич');

-- --------------------------------------------------------

--
-- Table structure for table `school_teacher_student`
--

CREATE TABLE IF NOT EXISTS `school_teacher_student` (
  `teacher_id` int(11) NOT NULL DEFAULT '0',
  `student_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`teacher_id`,`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `school_teacher_student`
--

INSERT INTO `school_teacher_student` (`teacher_id`, `student_id`) VALUES
(19, 3),
(20, 5),
(20, 6),
(21, 3),
(21, 4),
(21, 5),
(21, 6),
(21, 7),
(22, 3),
(22, 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
/**
 * @var StudentController $this
 * @var Teacher           $teacher
 */
?>

<h1>Добавление учителя</h1>

<div class="form">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'enableAjaxValidation' => false,
	)); ?>
	<?php echo $form->errorSummary($teacher); ?>

	<div class="row">
		<?php echo $form->labelEx($teacher, 'name'); ?>
		<?php echo $form->textField($teacher, 'name', array('size' => 60, 'maxlength' => 255)); ?>
		<?php echo $form->error($teacher, 'name'); ?>
	</div>

	<label>Назначить учеников</label>
	<?=
	CHtml::dropDownList('student[]', '', CHtml::listData($student, 'id', 'name'), array(
		'multiple' => 'multiple',
		'style'    => 'width: 452px;',
	));
	?>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Добавить'); ?>
	</div>

	<?php $this->endWidget(); ?>

</div>
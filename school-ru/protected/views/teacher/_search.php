<?php
/**
 * @var TeacherController $this
 * @var Teacher           $teacher
 */
?>

<div class="wide form">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'action' => Yii::app()->createUrl($this->route),
		'method' => 'get',
	)); ?>

	<div class="row">
		<?php echo $form->label($teacher, 'id'); ?>
		<?php echo $form->textField($teacher, 'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($teacher, 'name'); ?>
		<?php echo $form->textField($teacher, 'name', array('size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Найти'); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- search-form -->
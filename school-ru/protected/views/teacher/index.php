<?php
/**
 * @var TeacherController $this
 * @var Teacher           $teacher
 */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#teacher-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Учителя</h1>

<?php echo CHtml::link('Расширенный поиск', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search', array(
		'teacher' => $teacher,
	)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'           => 'teacher-grid',
	'dataProvider' => $teacher->search(),
	'filter'       => $teacher,
	'columns'      => array(
		'id',
		'name',
		'count_students',
		array(
			'class'    => 'CButtonColumn',
			'template' => '{update}{delete}',
		),
	),
)); ?>

<a href="<?= $this->createUrl('/teacher/create'); ?>">Добавить</a>
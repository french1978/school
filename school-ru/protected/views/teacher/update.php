<?php
/**
 * @var TeacherController $this
 * @var Teacher           $teacher
 * @var Student           $student
 */
?>

<h1>Редактирование учителя: "<?php echo $teacher->name; ?>"</h1>

<div class="form">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'enableAjaxValidation' => false,
	)); ?>
	<?php echo $form->errorSummary($teacher); ?>

	<div class="row">
		<?php echo $form->labelEx($teacher, 'name'); ?>
		<?php echo $form->textField($teacher, 'name', array('size' => 60, 'maxlength' => 255)); ?>
		<?php echo $form->error($teacher, 'name'); ?>
	</div>

	<label>Назначенные ученики</label>
	<?=
	CHtml::dropDownList('student[]', StudentHelper::selectStudent($teacher->students), CHtml::listData($student, 'id', 'name'), array(
		'multiple' => 'multiple',
		'style'    => 'width: 452px;',
	));
	?>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Обновить'); ?>
	</div>

	<?php $this->endWidget(); ?>

</div>
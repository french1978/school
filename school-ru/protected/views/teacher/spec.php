<?php
/**
 * @var TeacherController $this
 * @var array             $spec
 * @var array             $best
 */
?>

<h1>Список учителей, с которыми занимаются только следующие ученики: Георгий, Харитон, Денис, Андрей</h1>

<?php if ($spec): ?>
	<table style="border: solid 1px #000;">
		<tr>
			<th style="border-bottom: solid 1px #000;">ID</th>
			<th style="border-bottom: solid 1px #000;">Имя</th>
		</tr>
		<?php foreach ($spec as $val): ?>
			<tr>
				<td style="border-bottom: solid 1px #000;"><?= $val['id']; ?></td>
				<td style="border-bottom: solid 1px #000;"><?= $val['name']; ?></td>
			</tr>
		<? endforeach; ?>
	</table>
<?php endif; ?>

<br><br>

<h1>Информация о любых двух учителях, у которых максимальное количество общих учеников</h1>

<?php if ($best): ?>
	<table style="border: solid 1px #000;">
		<tr>
			<th style="border-bottom: solid 1px #000;">ID</th>
			<th style="border-bottom: solid 1px #000;">Имя</th>
		</tr>
		<?php foreach ($best as $val): ?>
			<tr>
				<td style="border-bottom: solid 1px #000;"><?= $val['id']; ?></td>
				<td style="border-bottom: solid 1px #000;"><?= $val['name']; ?></td>
			</tr>
		<? endforeach; ?>
	</table>
<?php endif; ?>
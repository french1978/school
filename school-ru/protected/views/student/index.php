<?php
/**
 * @var StudentController $this
 * @var Student           $student
 */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#student-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Ученики</h1>

<?php echo CHtml::link('Расширенный поиск', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search', array(
		'student' => $student,
	)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'           => 'student-grid',
	'dataProvider' => $student->search(),
	'filter'       => $student,
	'columns'      => array(
		'id',
		'name',
		array(
			'class'    => 'CButtonColumn',
			'template' => '{update}{delete}',
		),
	),
)); ?>

<a href="<?= $this->createUrl('/student/create'); ?>">Добавить</a>
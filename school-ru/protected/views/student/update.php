<?php
/**
 * @var StudentController $this
 * @var Student           $student
 */
?>

<h1>Редактирование ученика: "<?php echo $student->name; ?>"</h1>

<div class="form">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'enableAjaxValidation' => false,
	)); ?>
	<?php echo $form->errorSummary($student); ?>

	<div class="row">
		<?php echo $form->labelEx($student, 'name'); ?>
		<?php echo $form->textField($student, 'name', array('size' => 60, 'maxlength' => 255)); ?>
		<?php echo $form->error($student, 'name'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Обновить'); ?>
	</div>

	<?php $this->endWidget(); ?>

</div>
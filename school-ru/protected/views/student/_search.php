<?php
/**
 * @var StudentController $this
 * @var Student           $student
 */
?>

<div class="wide form">

	<?php $form = $this->beginWidget('CActiveForm', array(
		'action' => Yii::app()->createUrl($this->route),
		'method' => 'get',
	)); ?>

	<div class="row">
		<?php echo $form->label($student, 'id'); ?>
		<?php echo $form->textField($student, 'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($student, 'name'); ?>
		<?php echo $form->textField($student, 'name', array('size' => 60, 'maxlength' => 255)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Найти'); ?>
	</div>

	<?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
/* @var ErrorController $this */
/* @var string $message */

$this->pageTitle = '404';
?>

<h2>Ошибка <?php echo $code; ?></h2>

<div class="error">
	<?php echo CHtml::encode($message); ?>
</div>
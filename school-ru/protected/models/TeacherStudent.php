<?php

/**
 * This is the model class for table "student".
 * The followings are the available columns in table 'student':
 * @property integer $id
 * @property string  $name
 * @property string  $teacher_id
 */
class TeacherStudent extends CActiveRecord {

	/**
	 * Имя таблици
	 * @return string
	 */
	public function tableName() {
		return '{{teacher_student}}';
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

}

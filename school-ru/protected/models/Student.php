<?php

/**
 * Модель учеников
 * @property int    $id    ID
 * @property string $name  Имя
 */
class Student extends CActiveRecord
{

	const STUD_GEORGY  = 'Георгий';
	const STUD_HARITON = 'Харитон';
	const STUD_DENIS   = 'Денис';
	const STUD_ANDREY  = 'Андрей';

	/**
	 * Название таблицы
	 * @return string
	 */
	public function tableName() {
		return '{{student}}';
	}

	/**
	 * Валидация
	 * @return array
	 */
	public function rules() {
		return array(
			array('name', 'required'),
			array('name', 'length', 'max' => 255),
			array('id, name', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * Алиасы
	 * @return array
	 */
	public function attributeLabels() {
		return array(
			'id'   => 'ID',
			'name' => 'Имя',
		);
	}

	/**
	 * Поиск
	 * @return CActiveDataProvider
	 */
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string $className
	 * @return mixed
	 */
	public static function model($className = __CLASS__) {
		return parent::model($className);
	}

}
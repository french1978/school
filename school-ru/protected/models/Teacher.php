<?php

/**
 * Модель учитилей
 * @property int    $id    ID
 * @property string $name  Имя
 */
class Teacher extends CActiveRecord
{

	/**
	 * Назначенные учениеи
	 * @var array
	 */
	public $student = array();

	/**
	 * Название таблици
	 * @return string
	 */
	public function tableName() {
		return '{{teacher}}';
	}

	/**
	 * Валидация
	 * @return array
	 */
	public function rules() {
		return array(
			array('name', 'required'),
			array('name', 'length', 'max' => 255),
			array('id, name', 'safe', 'on' => 'search'),
		);
	}

	/**
	 * Функция описывающая связи моделей
	 * @return array
	 */
	public function relations() {
		return array(
			'count_students' => array(self::STAT, 'TeacherStudent', 'teacher_id'),
			'students'       => array(self::HAS_MANY, 'TeacherStudent', 'teacher_id'),
		);
	}

	/**
	 * Алиасы
	 * @return array
	 */
	public function attributeLabels() {
		return array(
			'id'             => 'ID',
			'name'           => 'Имя',
			'count_students' => 'Назначенные ученики',
		);
	}

	/**
	 * Поиск
	 * @return CActiveDataProvider
	 */
	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('name', $this->name, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
		));
	}

	/**
	 * @param string $className
	 * @return mixed
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * После сохранения добавляем связи в TeacherStudent
	 */
	protected function afterSave() {
		TeacherStudent::model()->deleteAll('teacher_id = :teacher_id', array(':teacher_id' => $this->id));
		if ($this->student) {
			foreach ($this->student as $student) {
				$teacher_student             = new TeacherStudent;
				$teacher_student->student_id = $student;
				$teacher_student->teacher_id = $this->id;
				$teacher_student->save();
			}
		}
	}

	/**
	 * После удаления, чистим все связи
	 */
	protected function afterDelete() {
		TeacherStudent::model()->deleteAll('teacher_id = :teacher_id', array(':teacher_id' => $this->id));
	}

	/**
	 * @todo вообще тут нужно по ID выборку делать, но я по ТЗ понял что именно по именам
	 * Выбираем учителей, занимающихся с определёнными учениками
	 * @return null|array
	 */
	public function findAllBySpec() {
		$sql = '
			SELECT t.*
			FROM {{teacher}} t
			INNER JOIN {{teacher_student}} ts ON t.id = ts.teacher_id
			INNER JOIN {{student}} s ON s.id = ts.student_id
			AND s.name IN(
				"' . Student::STUD_ANDREY . '",
				"' . Student::STUD_HARITON . '",
				"' . Student::STUD_DENIS . '",
				"' . Student::STUD_GEORGY . '"
				)
			GROUP BY t.id
		';
		return Yii::app()->db->createCommand($sql)->queryAll();
	}

	/**
	 * Выбираем двух учителей, у которых максимальное количество общих учеников
	 * @return null|array
	 */
	public function findAllByBest() {
		$sql = '
			SELECT t.name, t.id, ts1.teacher_id, COUNT(*) AS count
			FROM school_teacher_student AS ts1
			INNER JOIN school_teacher_student AS ts2 ON ts1.student_id = ts2.student_id
			INNER JOIN school_teacher AS t ON ts1.teacher_id = t.id
			GROUP BY ts1.teacher_id
			ORDER BY count DESC
			LIMIT 0, 2
		';
		return Yii::app()->db->createCommand($sql)->queryAll();
	}

}
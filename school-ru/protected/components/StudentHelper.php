<?php

class StudentHelper {

	/**
	 * Формируем массив для селекта
	 * @param $students
	 * @return array|null
	 */
	public static function selectStudent($students) {
		if (!$students) {
			return null;
		}
		$select_students = array();
		foreach ($students as $val) {
			$select_students[] = $val['student_id'];
		}
		return $select_students;
	}

}
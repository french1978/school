<?php

class TeacherController extends Controller
{

	/**
	 * Весь список
	 */
	public function actionIndex() {
		$teacher = new Teacher('search');
		$teacher->unsetAttributes();
		if (isset($_GET['Teacher'])) {
			$teacher->attributes = $_GET['Teacher'];
		}

		$this->render('index', array(
			'teacher' => $teacher,
		));
	}

	/**
	 * Добавление учителя
	 */
	public function actionCreate() {
		$teacher = new Teacher;
		$student = Student::model()->findAll();

		if (isset($_POST['Teacher'])) {
			$teacher->attributes = $_POST['Teacher'];
			$teacher->student    = $_POST['student'];

			if ($teacher->save()) {
				$this->redirect(array('/teacher'));
			}
		}

		$this->render('create', array(
			'teacher' => $teacher,
			'student' => $student,
		));
	}

	/**
	 * Редактирование учителя
	 * @param int $id
	 */
	public function actionUpdate($id) {
		$teacher = Teacher::model()->with(array('students'))->findByPk($id);
		$student = Student::model()->findAll();

		if (isset($_POST['Teacher'])) {
			$teacher->attributes = $_POST['Teacher'];
			$teacher->student    = $_POST['student'];

			if ($teacher->save()) {
				$this->redirect(array('/teacher'));
			}
		}

		$this->render('update', array(
			'teacher' => $teacher,
			'student' => $student,
		));
	}

	/**
	 * Удаление
	 * @param int $id
	 */
	public function actionDelete($id) {
		$this->loadModel($id)->delete();

		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	/**
	 * Списки учителей с фильтром
	 */
	public function actionSpec() {
		$spec = Teacher::model()->findAllBySpec();
		$best = Teacher::model()->findAllByBest();

		$this->render('spec', array(
			'spec' => $spec,
			'best' => $best,
		));
	}

	/**
	 * Загружаем модель, либо эксэпшн
	 * @param int $id
	 * @return mixed
	 * @throws CHttpException
	 */
	public function loadModel($id) {
		$model = Teacher::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'Не найдена страница.');
		}
		return $model;
	}

}
<?php

class StudentController extends Controller
{

	/**
	 * Списом учеников
	 */
	public function actionIndex() {
		$student = new Student('search');
		$student->unsetAttributes();
		if (isset($_GET['Student'])) {
			$student->attributes = $_GET['Student'];
		}

		$this->render('index', array(
			'student' => $student,
		));
	}

	/**
	 * Добасление ученика
	 */
	public function actionCreate() {
		$student = new Student;

		if (isset($_POST['Student'])) {
			$student->attributes = $_POST['Student'];
			if ($student->save()) {
				$this->redirect(array('/student'));
			}
		}

		$this->render('create', array(
			'student' => $student,
		));
	}

	/**
	 * Редактирование ученике
	 * @param int $id
	 */
	public function actionUpdate($id) {
		$student = $this->loadModel($id);

		if (isset($_POST['Student'])) {
			$student->attributes = $_POST['Student'];
			if ($student->save()) {
				$this->redirect(array('/student'));
			}
		}

		$this->render('update', array(
			'student' => $student,
		));
	}

	/**
	 * Удаление
	 * @param int $id
	 */
	public function actionDelete($id) {
		$this->loadModel($id)->delete();

		if (!isset($_GET['ajax'])) {
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
	}

	/**
	 * Загружаем модель, либо эксэпшн
	 * @param int $id
	 * @return mixed
	 * @throws CHttpException
	 */
	public function loadModel($id) {
		$model = Student::model()->findByPk($id);
		if ($model === null) {
			throw new CHttpException(404, 'Нет такой страници.');
		}
		return $model;
	}

}